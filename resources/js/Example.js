import React, { useEffect } from 'react';
import { useStoreActions } from 'easy-peasy';

import AddTodos from './components/Todo/AddTodos';
import Todos from './components/Todo/Todos';

const Example = () => {
    const fetchTodosAction = useStoreActions(actions => actions.fetchTodos)
    useEffect(() => {
        fetchTodosAction()
    }, [])

    return (
        <div className="container mt-5">
            <Todos />
            <AddTodos />
        </div>
    )
}

export default Example
