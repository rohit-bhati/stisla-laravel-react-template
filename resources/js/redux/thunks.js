import todoThunks from './Todo/thunks'

export default {
    ...todoThunks
}
