import state from './state'
import actions from './actions'
import thunks from './thunks';

export default { ...state, ...actions, ...thunks }
