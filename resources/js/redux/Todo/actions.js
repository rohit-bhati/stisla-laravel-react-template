import { action } from 'easy-peasy'
import uuid from 'uuid'

export default {
    setTodos: action((state, todos) => {
        state.todos = todos
    }),
    toggleTodo: action((state, id) => {
        state.todos.map(todo => {
            if (todo.id === id) todo.completed = !todo.completed
            return todo
        })
    }),
    addTodo: action((state, todo) => {
        state.todos = [...state.todos, { ...todo, id: uuid.v4() }]
    }),
    deleteTodo: action((state, id) => {
        state.todos = state.todos.filter(todo => todo.id !== id)
    })
}
