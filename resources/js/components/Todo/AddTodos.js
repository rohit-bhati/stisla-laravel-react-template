import React, { useState } from 'react'
import { useStoreActions } from 'easy-peasy';

const AddTodos = () => {
    const [todo, setTodo] = useState({ title: '', completed: false })
    const addTodoAction = useStoreActions(actions => actions.addTodo)

    const inputRef = React.createRef()

    const addTodoHandler = () => {
        addTodoAction(todo)
        inputRef.current.value = ''
        inputRef.current.focus()
    }
    return (
        <div className="form-group">
            <input
                type="text"
                className="form-control"
                name="add_todo"
                id="add_todo"
                ref={inputRef}
                onChange={(e) => setTodo({...todo, title: e.target.value})}
                placeholder="Add Todo"
            />
            <button onClick={addTodoHandler} type="button" className="btn btn-success">
                Add
            </button>
        </div>
    )
}

export default AddTodos
