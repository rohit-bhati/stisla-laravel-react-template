import React, { Fragment, useEffect } from 'react'
import { useStoreState, useStoreActions } from 'easy-peasy'
import TodoItem from './TodoItem'

const Todos = () => {
    const todos = useStoreState(state => state.todos)

    return (
        <Fragment>
            <h1>Todos</h1>
            <p>Click TODO Item to toggle completed</p>
            <hr />
            {todos.map(todo => (
                <TodoItem todo={todo} key={todo.id} />
            ))}
        </Fragment>
    )
}

export default Todos
