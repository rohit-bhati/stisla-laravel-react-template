import React, { Fragment } from 'react'
import { useStoreActions } from 'easy-peasy'

const TodoItem = ({ todo }) => {
    const { toggleTodoAction, deleteTodoAction } = useStoreActions(actions => ({
        toggleTodoAction: actions.toggleTodo,
        deleteTodoAction: actions.deleteTodo
    }))

    return (
        <Fragment>
            <div
                style={{
                    textDecoration: todo.completed ? 'line-through' : 'none'
                }}
            >
                <span
                    onClick={() => toggleTodoAction(todo.id)}
                    style={{ cursor: 'pointer' }}
                >
                    {todo.title}
                </span>
                <button onClick={() => deleteTodoAction(todo.id)}>Delete</button>
            </div>
        </Fragment>
    )
}

export default TodoItem
