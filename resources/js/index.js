import React from "react";
import ReactDOM from "react-dom";
import { StoreProvider, createStore } from "easy-peasy";

import Example from "./Example";
import model from './redux'

const store = createStore(model)

if (document.getElementById('app')) {
    ReactDOM.render(
        <StoreProvider store={store}>
            <Example />
        </StoreProvider>
    , document.getElementById('app'));
}
